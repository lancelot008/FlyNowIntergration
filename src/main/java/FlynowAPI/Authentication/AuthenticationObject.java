package FlynowAPI.Authentication;

public class AuthenticationObject {

    private DataObject Data;
    private boolean IsError;
    private int ErrorCode;
    private String Error;

    public AuthenticationObject() {
    }

    public DataObject getData() {
        return Data;
    }

    public void setData(DataObject data) {
        Data = data;
    }

    public boolean getIsError() {
        return IsError;
    }

    public void setIsError(boolean isError) {
        IsError = isError;
    }

    public int getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(int errorCode) {
        ErrorCode = errorCode;
    }

    public String getError() {
        return Error;
    }

    public void setError(String error) {
        Error = error;
    }
}
