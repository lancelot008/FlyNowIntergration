package FlynowAPI.Authentication;

import FlynowAPI.AgencyParams;
import FlynowAPI.MappingObjects.PostRequest;
import Utils.Utils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class Authentication extends PostRequest{



    private AuthenticationObject authenticationObject;
    private AgencyParams agencyParams;
    private String scheme;
    private String host;
    private String path;


    public Authentication(AgencyParams agencyParams) {
        this.agencyParams = agencyParams;
    }

    public AuthenticationObject getAuthenticationObject() {
        return authenticationObject;
    }

    public void setAuthenticationObject(AuthenticationObject authenticationObject) {
        this.authenticationObject = authenticationObject;
    }

    public AgencyParams getAgencyParams() {
        return agencyParams;
    }

    public void setAgencyParams(AgencyParams agencyParams) {
        this.agencyParams = agencyParams;
    }


    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setRequestInput() throws URISyntaxException {

        this.client = HttpClientBuilder.create().build();
        URI uri = new URIBuilder()
                .setScheme(getScheme())
                .setHost(getHost())
                .setPath(getPath())
                .build();
        this.httpost = new HttpPost(uri);
        String password_md5 = Utils.toHexString(Utils.md5(agencyParams.getPassword().getBytes()));
        String key_authen = Utils.toHexString(Utils.md5((agencyParams.getAccount() + password_md5 + agencyParams.getClient_key()).getBytes()));
        String body_request = "account=" + agencyParams.getAccount() + "&password=" + password_md5 + "&key=" + key_authen;
        StringEntity requestEntity = new StringEntity(body_request, ContentType.TEXT_PLAIN);
        this.httpost.setEntity(requestEntity);
    }



    public void mapping() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            JsonNode jsonNode = objectMapper.readTree(this.result_post.toString());
            assert jsonNode != null;
            this.setAuthenticationObject(objectMapper.readValue(jsonNode.toString(), AuthenticationObject.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
