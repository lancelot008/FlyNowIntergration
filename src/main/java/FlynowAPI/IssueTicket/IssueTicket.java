package FlynowAPI.IssueTicket;

import FlynowAPI.MappingObjects.GetRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class IssueTicket extends GetRequest{

    private TicketObject ticketObject;

    private String Airline;
    private String bookingCode;
    private String access_token;

    public IssueTicket(String airline, String bookingCode, String access_token) {
        Airline = airline;
        this.bookingCode = bookingCode;
        this.access_token = access_token;
    }

    public TicketObject getTicketObject() {
        return ticketObject;
    }

    public void setTicketObject(TicketObject ticketObject) {
        this.ticketObject = ticketObject;
    }

    public String getAirline() {
        return Airline;
    }

    public void setAirline(String airline) {
        Airline = airline;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    @Override
    public void setRequestInput() throws URISyntaxException {
        super.setRequestInput();

        client = HttpClientBuilder.create().build();
        URI uri = new URIBuilder()
                .setScheme(getScheme())
                .setHost(getHost())
                .setPath(getPath())
                .addParameter("Airline", getAirline())
                .addParameter("bookingCode", getBookingCode())
                .build();


        httpGet = new HttpGet(uri);
        httpGet.addHeader("Content-Type", "application/x-www-form-urlencoded");
        httpGet.addHeader("token", "Bearer " + this.access_token);
    }

    @Override
    public void mapping() {
        super.mapping();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            JsonNode jsonNode = objectMapper.readTree(this.stringBuffer.toString());
            assert jsonNode != null;
            this.setTicketObject(objectMapper.readValue(jsonNode.toString(), TicketObject.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
