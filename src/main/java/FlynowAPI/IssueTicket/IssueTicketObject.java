package FlynowAPI.IssueTicket;

public class IssueTicketObject {


    private long index;
    private String Airline;
    private String TicketNumber;
    private String IssueDate;
    private String BookingCode;
    private String PassengerName;
    private String BoolingFile;
    private String TicketImage;


    public IssueTicketObject() {
    }


    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getAirline() {
        return Airline;
    }

    public void setAirline(String airline) {
        Airline = airline;
    }

    public String getTicketNumber() {
        return TicketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        TicketNumber = ticketNumber;
    }

    public String getIssueDate() {
        return IssueDate;
    }

    public void setIssueDate(String issueDate) {
        IssueDate = issueDate;
    }

    public String getBookingCode() {
        return BookingCode;
    }

    public void setBookingCode(String bookingCode) {
        BookingCode = bookingCode;
    }

    public String getPassengerName() {
        return PassengerName;
    }

    public void setPassengerName(String passengerName) {
        PassengerName = passengerName;
    }

    public String getBoolingFile() {
        return BoolingFile;
    }

    public void setBoolingFile(String boolingFile) {
        BoolingFile = boolingFile;
    }

    public String getTicketImage() {
        return TicketImage;
    }

    public void setTicketImage(String ticketImage) {
        TicketImage = ticketImage;
    }
}
