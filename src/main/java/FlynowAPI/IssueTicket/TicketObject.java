package FlynowAPI.IssueTicket;

import java.util.List;

public class TicketObject {

    private List<IssueTicketObject> IssueTicket;

    private String ErrorMessage;
    private int StatusCode;


    public TicketObject() {
    }

    public List<IssueTicketObject> getIssueTicket() {
        return IssueTicket;
    }

    public void setIssueTicket(List<IssueTicketObject> issueTicket) {
        IssueTicket = issueTicket;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        ErrorMessage = errorMessage;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }
}
