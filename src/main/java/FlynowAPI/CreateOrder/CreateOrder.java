package FlynowAPI.CreateOrder;

import FlynowAPI.AgencyParams;
import FlynowAPI.MappingObjects.PostRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

public class CreateOrder extends PostRequest{
    private CreateOrderObject createOrderObject;
    private AgencyParams agencyParams;
    private String guestInforJsonString;
    private List<GuestInforObject> guestInfor;
    private ContactInfoObject contactInfo;
    private FnSelectObject fnSelect;
    private String contactInfoJsonString;
    private String fnSelectJsonString;
    private String orderId;
    private String access_token;

    public CreateOrder(List<GuestInforObject> guestInfor, ContactInfoObject contactInfo, FnSelectObject fnSelect, AgencyParams agencyParams, String orderId, String access_token) {
        this.guestInfor = guestInfor;
        this.contactInfo = contactInfo;
        this.fnSelect = fnSelect;
        this.agencyParams = agencyParams;
        this.orderId = orderId;
        this.access_token = access_token;
    }

    public CreateOrderObject getCreateOrderObject() {
        return createOrderObject;
    }

    public void setCreateOrderObject(CreateOrderObject createOrderObject) {
        this.createOrderObject = createOrderObject;
    }

    public List<GuestInforObject> getGuestInfor() {
        return guestInfor;
    }

    public void setGuestInfor(List<GuestInforObject> guestInfor) {
        this.guestInfor = guestInfor;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public void setRequestInput() throws URISyntaxException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            guestInforJsonString = mapper.writeValueAsString(guestInfor);
            contactInfoJsonString = mapper.writeValueAsString(contactInfo);
            fnSelectJsonString = mapper.writeValueAsString(fnSelect);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        String request_body = "{ \"aId\": \"" + agencyParams.getaId() + "\", \"OrderId\":" + getOrderId() + ", \"guestInfor\":" + guestInforJsonString + ", \"contactInfo\":" +  contactInfoJsonString + ",\"fnSelect\":" + fnSelectJsonString + "}";

        System.out.println(request_body);

        this.client = HttpClientBuilder.create().build();

        URI uri = new URIBuilder()
                .setScheme(getScheme())
                .setHost(getHost())
                .setPath(getPath())
                .build();
        this.httpost = new HttpPost(uri);
        StringEntity requestEntity = new StringEntity(request_body, ContentType.APPLICATION_JSON);
        this.httpost.setEntity(requestEntity);
        this.httpost.addHeader("Content-Type", "application/json");
        this.httpost.addHeader("token", "Bearer " + this.access_token);

    }

    public void mapping() {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

            JsonNode jsonNode = objectMapper.readTree(this.result_post.toString());

            assert jsonNode != null;
            this.setCreateOrderObject(objectMapper.readValue(jsonNode.toString(), CreateOrderObject.class));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
