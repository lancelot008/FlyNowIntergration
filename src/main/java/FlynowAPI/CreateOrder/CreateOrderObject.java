package FlynowAPI.CreateOrder;

import java.util.List;

public class CreateOrderObject {

    private String HoldId;
    private String OrderId;
    private String ErrorMessages;
    private String StatusCode;
    private List<PriceDetailObject> PriceDetail;

    public String getHoldId() {
        return HoldId;
    }

    public void setHoldId(String holdId) {
        HoldId = holdId;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getErrorMessages() {
        return ErrorMessages;
    }

    public void setErrorMessages(String errorMessages) {
        ErrorMessages = errorMessages;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }

    public List<PriceDetailObject> getPriceDetail() {
        return PriceDetail;
    }

    public void setPriceDetail(List<PriceDetailObject> priceDetail) {
        PriceDetail = priceDetail;
    }
}
