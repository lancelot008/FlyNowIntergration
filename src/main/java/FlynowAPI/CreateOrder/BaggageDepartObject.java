package FlynowAPI.CreateOrder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaggageDepartObject {

    private String AirlineCode;
    private String Code;
    private String Currency;
    private String Name;
    private String Price;
    private String Value;


    public BaggageDepartObject(String airlineCode, String code, String currency, String name, String price, String value) {
        AirlineCode = airlineCode;
        Code = code;
        Currency = currency;
        Name = name;
        Price = price;
        Value = value;
    }

    @JsonProperty("AirlineCode")
    public String getAirlineCode() {
        return AirlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        AirlineCode = airlineCode;
    }

    @JsonProperty("Code")
    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    @JsonProperty("Currency")
    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    @JsonProperty("Name")
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @JsonProperty("Price")
    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    @JsonProperty("Value")
    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
