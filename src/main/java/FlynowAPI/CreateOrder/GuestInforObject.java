package FlynowAPI.CreateOrder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GuestInforObject {

    private long Id;
    private String Birthday;
    private int Type;
    private String Title;
    private int Sex;
    private String FirstName;
    private String MiddleName;
    private String LastName;
    private String PassportNo;
    private String ExpiredDate;
    private String PNRDepart;
    private String PNRArrival;
    private String CodeTicketDepart;
    private String CodeTicketArrival;
    private BaggageDepartObject BaggageDepart;
    private BaggageReturnObject BaggageReturn;

    public GuestInforObject(long Id, String Birthday, int Type, String Title, int Sex, String FirstName, String MiddleName, String LastName, String PassportNo, String ExpiredDate, String PNRDepart, String PNRArrival, String CodeTicketDepart, String CodeTicketArrival, BaggageDepartObject BaggageDepart, BaggageReturnObject BaggageReturn) {

        this.Id = Id;
        this.Birthday = Birthday;
        this.Type = Type;
        this.Title = Title;
        this.Sex = Sex;
        this.FirstName = FirstName;
        this.MiddleName = MiddleName;
        this.LastName = LastName;
        this.PassportNo = PassportNo;
        this.ExpiredDate = ExpiredDate;
        this.PNRDepart = PNRDepart;
        this.PNRArrival = PNRArrival;
        this.CodeTicketDepart = CodeTicketDepart;
        this.CodeTicketArrival = CodeTicketArrival;
        this.BaggageDepart = BaggageDepart;
        this.BaggageReturn = BaggageReturn;
    }

    @JsonProperty("Id")
    public long getId() {
        return Id;
    }

    public void setId(long Id) {
        this.Id = Id;
    }
    @JsonProperty("Birthday")
    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String Birthday) {
        this.Birthday = Birthday;
    }

    @JsonProperty("Type")
    public int getType() {
        return Type;
    }

    public void setType(int Type) {
        this.Type = Type;
    }
    @JsonProperty("Title")
    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Type = Type;
    }

    @JsonProperty("Sex")
    public int getSex() {
        return Sex;
    }

    public void setSex(int Sex) {
        this.Sex = Sex;
    }


    @JsonProperty("FirstName")
    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }


    @JsonProperty("MiddleName")
    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String MiddleName) {
        this.MiddleName = MiddleName;
    }

    @JsonProperty("LastName")
    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    @JsonProperty("PassportNo")
    public String getPassportNo() {
        return PassportNo;
    }

    public void setPassportNo(String PassportNo) {
        this.PassportNo = PassportNo;
    }

    @JsonProperty("ExpiredDate")
    public String getExpiredDate() {
        return ExpiredDate;
    }

    public void setExpiredDate(String ExpiredDate) {
        this.ExpiredDate = ExpiredDate;
    }

    @JsonProperty("PNRDepart")
    public String getPNRDepart() {
        return PNRDepart;
    }

    public void setPNRDepart(String PNRDepart) {
        this.PNRDepart = PNRDepart;
    }

    @JsonProperty("PNRArrival")
    public String getPNRArrival() {
        return PNRArrival;
    }

    public void setPNRArrival(String PNRArrival) {
        this.PNRArrival = PNRArrival;
    }

    @JsonProperty("CodeTicketDepart")
    public String getCodeTicketDepart() {
        return CodeTicketDepart;
    }

    public void setCodeTicketDepart(String CodeTicketDepart) {
        this.CodeTicketDepart = CodeTicketDepart;
    }

    @JsonProperty("CodeTicketArrival")
    public String getCodeTicketArrival() {
        return CodeTicketArrival;
    }

    public void setCodeTicketArrival(String CodeTicketArrival) {
        this.CodeTicketArrival = CodeTicketArrival;
    }

    @JsonProperty("BaggageDepart")
    public BaggageDepartObject getBaggageDepart() {
        return BaggageDepart;
    }

    public void setBaggageDepart(BaggageDepartObject BaggageDepart) {
        this.BaggageDepart = BaggageDepart;
    }

    @JsonProperty("BaggageReturn")
    public BaggageReturnObject getBaggageReturn() {
        return BaggageReturn;
    }

    public void setBaggageReturn(BaggageReturnObject BaggageReturn) {
        this.BaggageReturn = BaggageReturn;
    }
}
