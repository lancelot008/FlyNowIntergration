package FlynowAPI.CreateOrder;

public class PriceDetailObject {

    private String Airline;
    private double Price;

    public PriceDetailObject() {
    }

    public String getAirline() {
        return Airline;
    }

    public void setAirline(String airline) {
        Airline = airline;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }
}
