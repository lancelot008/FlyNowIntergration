package FlynowAPI.CreateOrder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContactInfoObject {

    private String Name;
    private String Address;
    private String Phone;
    private String Email;
    private String ReceivePlace;
    private String CompanyAddress;
    private String TaxNumber;
    private String CompanyName;
    private String City;

    public ContactInfoObject(String name, String address, String phone, String email, String receivePlace, String companyAddress, String taxNumber, String companyName, String city) {
        Name = name;
        Address = address;
        Phone = phone;
        Email = email;
        ReceivePlace = receivePlace;
        CompanyAddress = companyAddress;
        TaxNumber = taxNumber;
        CompanyName = companyName;
        City = city;
    }

    @JsonProperty("Name")
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @JsonProperty("Address")
    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    @JsonProperty("Phone")
    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    @JsonProperty("Email")
    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    @JsonProperty("ReceivePlace")
    public String getReceivePlace() {
        return ReceivePlace;
    }

    public void setReceivePlace(String receivePlace) {
        ReceivePlace = receivePlace;
    }

    @JsonProperty("CompanyAddress")
    public String getCompanyAddress() {
        return CompanyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        CompanyAddress = companyAddress;
    }

    @JsonProperty("TaxNumber")
    public String getTaxNumber() {
        return TaxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        TaxNumber = taxNumber;
    }

    @JsonProperty("CompanyName")
    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    @JsonProperty("City")
    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }
}
