package FlynowAPI.CreateOrder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FnSelectObject {

    private long UID;
    private long OutboundID;
    private long OutboundPriceID;
    private int ItineraryType;
    private long InboundID;
    private long InboundPriceID;

    public FnSelectObject(long UID, long outboundID, long outboundPriceID, int itineraryType, long inboundID, long inboundPriceID) {
        this.UID = UID;
        OutboundID = outboundID;
        OutboundPriceID = outboundPriceID;
        ItineraryType = itineraryType;
        InboundID = inboundID;
        InboundPriceID = inboundPriceID;
    }

    @JsonProperty("UID")
    public long getUID() {
        return UID;
    }

    public void setUID(long UID) {
        this.UID = UID;
    }

    @JsonProperty("OutboundID")
    public long getOutboundID() {
        return OutboundID;
    }

    public void setOutboundID(long outboundID) {
        OutboundID = outboundID;
    }

    @JsonProperty("OutboundPriceID")
    public long getOutboundPriceID() {
        return OutboundPriceID;
    }

    public void setOutboundPriceID(long outboundPriceID) {
        OutboundPriceID = outboundPriceID;
    }

    @JsonProperty("ItineraryType")
    public int getItineraryType() {
        return ItineraryType;
    }

    public void setItineraryType(int itineraryType) {
        ItineraryType = itineraryType;
    }

    @JsonProperty("InboundID")
    public long getInboundID() {
        return InboundID;
    }

    public void setInboundID(long inboundID) {
        InboundID = inboundID;
    }

    @JsonProperty("InboundPriceID")
    public long getInboundPriceID() {
        return InboundPriceID;
    }

    public void setInboundPriceID(long inboundPriceID) {
        InboundPriceID = inboundPriceID;
    }
}
