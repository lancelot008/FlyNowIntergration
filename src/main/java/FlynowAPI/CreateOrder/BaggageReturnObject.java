package FlynowAPI.CreateOrder;

public class BaggageReturnObject extends BaggageDepartObject{

    public BaggageReturnObject(String airlineCode, String code, String currency, String name, String price, String value) {
        super(airlineCode, code, currency, name, price, value);
    }
}
