package FlynowAPI.GetStatusByHoldId;

import FlynowAPI.GetAddOnService.GetAddOnServiceObject;
import FlynowAPI.MappingObjects.GetRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class GetStatusByHoldId extends GetRequest{

    private GetStatusByHoldIdObject getStatusByHoldIdObject;
    private String holdId;
    private String access_token;

    public GetStatusByHoldId(String holdId, String access_token) {
        this.holdId = holdId;
        this.access_token = access_token;
    }

    public GetStatusByHoldIdObject getGetStatusByHoldIdObject() {
        return getStatusByHoldIdObject;
    }

    public void setGetStatusByHoldIdObject(GetStatusByHoldIdObject getStatusByHoldIdObject) {
        this.getStatusByHoldIdObject = getStatusByHoldIdObject;
    }


    public String getHoldId() {
        return holdId;
    }

    public void setHoldId(String holdId) {
        this.holdId = holdId;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    @Override
    public void setRequestInput() throws URISyntaxException {
        super.setRequestInput();

        client = HttpClientBuilder.create().build();
        URI uri = new URIBuilder()
                .setScheme(getScheme())
                .setHost(getHost())
                .setPath(getPath())
                .addParameter("holdId", getHoldId())
                .build();


        httpGet = new HttpGet(uri);
        httpGet.addHeader("Content-Type", "application/x-www-form-urlencoded");
        httpGet.addHeader("token", "Bearer " + this.access_token);
    }

    @Override
    public void mapping() {
        super.mapping();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            JsonNode jsonNode = objectMapper.readTree(this.stringBuffer.toString());
            assert jsonNode != null;
            this.setGetStatusByHoldIdObject(objectMapper.readValue(jsonNode.toString(), GetStatusByHoldIdObject.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
