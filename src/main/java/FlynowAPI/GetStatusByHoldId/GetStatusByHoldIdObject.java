package FlynowAPI.GetStatusByHoldId;
import java.util.List;

public class GetStatusByHoldIdObject{

    private List<BookingStatusObject> bookingStatusObjects;

    public GetStatusByHoldIdObject() {
    }


    public List<BookingStatusObject> getBookingStatusObjects() {
        return bookingStatusObjects;
    }

    public void setBookingStatusObjects(List<BookingStatusObject> bookingStatusObjects) {
        this.bookingStatusObjects = bookingStatusObjects;
    }

}
