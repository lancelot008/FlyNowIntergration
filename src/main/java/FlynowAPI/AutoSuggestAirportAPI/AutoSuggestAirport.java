package FlynowAPI.AutoSuggestAirportAPI;

import FlynowAPI.AgencyParams;
import FlynowAPI.MappingObjects.GetRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class AutoSuggestAirport extends GetRequest {

    private String keyword;
    private List<AutoSuggestAirportObject> autoSuggestAirportObjectList;
    private AgencyParams agencyParams;


    public AutoSuggestAirport(String keyword, AgencyParams agencyParams) {
        this.keyword = keyword;
        this.agencyParams = agencyParams;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public AgencyParams getAgencyParams() {
        return agencyParams;
    }

    public void setAgencyParams(AgencyParams agencyParams) {
        this.agencyParams = agencyParams;
    }

    public List<AutoSuggestAirportObject> getAutoSuggestAirportObjectList() {
        return autoSuggestAirportObjectList;
    }


    public void setAutoSuggestAirportObjectList(List<AutoSuggestAirportObject> autoSuggestAirportObjectList) {
        this.autoSuggestAirportObjectList = autoSuggestAirportObjectList;
    }

    public void setRequestInput() throws URISyntaxException {
        client = HttpClientBuilder.create().build();
        URI uri = new URIBuilder()
                .setScheme(getScheme())
                .setHost(getHost())
                .setPath(getPath())
                .addParameter("Search", getKeyword())
                .addParameter("aId", agencyParams.getaId())
                .build();

        httpGet = new HttpGet(uri);

    }

    public void mapping(){
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

        JsonNode jsonNode = null;
        try {
            jsonNode = objectMapper.readTree(stringBuffer.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<AutoSuggestAirportObject> autoSuggestAirportObjects = new ArrayList<>();

        Consumer<JsonNode> data = (JsonNode node) -> {
            try {
                AutoSuggestAirportObject autoSuggestAirportObject = objectMapper.readValue(node.toString(), AutoSuggestAirportObject.class);
                autoSuggestAirportObjects.add(autoSuggestAirportObject);
            } catch (IOException e) {
                e.printStackTrace();
            }

        };
        assert jsonNode != null;
        jsonNode.forEach(data);
        this.setAutoSuggestAirportObjectList(autoSuggestAirportObjects);
    }
}
