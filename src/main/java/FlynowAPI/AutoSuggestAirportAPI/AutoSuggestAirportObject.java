package FlynowAPI.AutoSuggestAirportAPI;

public class AutoSuggestAirportObject {

    private String PlaceId;
    private String PlaceName;
    private String PlaceNameEn;
    private double GeoId;
    private double GeoContainerId;
    private String OrderDisplay;
    private String CityName;
    private String CityNameEn;
    private String CityId;
    private String CountryName;
    private String CountryNameVN;
    private String CountryId;
    private String CountryNameNoSign;

    public AutoSuggestAirportObject() {
    }

    public String getPlaceId() {
        return PlaceId;
    }

    public void setPlaceId(String placeId) {
        PlaceId = placeId;
    }

    public String getPlaceName() {
        return PlaceName;
    }

    public void setPlaceName(String placeName) {
        PlaceName = placeName;
    }

    public String getPlaceNameEn() {
        return PlaceNameEn;
    }

    public void setPlaceNameEn(String placeNameEn) {
        PlaceNameEn = placeNameEn;
    }

    public double getGeoId() {
        return GeoId;
    }

    public void setGeoId(double geoId) {
        GeoId = geoId;
    }

    public double getGeoContainerId() {
        return GeoContainerId;
    }

    public void setGeoContainerId(double geoContainerId) {
        GeoContainerId = geoContainerId;
    }

    public String getOrderDisplay() {
        return OrderDisplay;
    }

    public void setOrderDisplay(String orderDisplay) {
        OrderDisplay = orderDisplay;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getCityNameEn() {
        return CityNameEn;
    }

    public void setCityNameEn(String cityNameEn) {
        CityNameEn = cityNameEn;
    }

    public String getCityId() {
        return CityId;
    }

    public void setCityId(String cityId) {
        CityId = cityId;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getCountryNameVN() {
        return CountryNameVN;
    }

    public void setCountryNameVN(String countryNameVN) {
        CountryNameVN = countryNameVN;
    }

    public String getCountryId() {
        return CountryId;
    }

    public void setCountryId(String countryId) {
        CountryId = countryId;
    }

    public String getCountryNameNoSign() {
        return CountryNameNoSign;
    }

    public void setCountryNameNoSign(String countryNameNoSign) {
        CountryNameNoSign = countryNameNoSign;
    }
}
