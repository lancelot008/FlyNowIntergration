package FlynowAPI.SearchFlightAPI;

import FlynowAPI.AgencyParams;
import FlynowAPI.MappingObjects.PostRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class SearchFlight extends PostRequest {

    private SearchFlightObject searchFlightObject;
    private String DepartureCode;
    private String ArrivalCode;
    private String DepartTime;
    private String ReturnTime;
    private String Adults;
    private String Child;
    private String Inf;
    private String IsRoundTrip;
    private AgencyParams agencyParams;


    public SearchFlight(String departureCode, String arrivalCode, String departTime, String returnTime, String adults, String child, String inf, String isRoundTrip, AgencyParams agencyParams) {
        DepartureCode = departureCode;
        ArrivalCode = arrivalCode;
        DepartTime = departTime;
        ReturnTime = returnTime;
        Adults = adults;
        Child = child;
        Inf = inf;
        IsRoundTrip = isRoundTrip;
        this.agencyParams = agencyParams;
    }

    public SearchFlightObject getSearchFlightObject() {
        return searchFlightObject;
    }

    public void setSearchFlightObject(SearchFlightObject searchFlightObject) {
        this.searchFlightObject = searchFlightObject;
    }

    public String getDepartureCode() {
        return DepartureCode;
    }

    public void setDepartureCode(String departureCode) {
        DepartureCode = departureCode;
    }

    public String getArrivalCode() {
        return ArrivalCode;
    }

    public void setArrivalCode(String arrivalCode) {
        ArrivalCode = arrivalCode;
    }

    public String getDepartTime() {
        return DepartTime;
    }

    public void setDepartTime(String departTime) {
        DepartTime = departTime;
    }

    public String getReturnTime() {
        return ReturnTime;
    }

    public void setReturnTime(String returnTime) {
        ReturnTime = returnTime;
    }

    public String getAdults() {
        return Adults;
    }

    public void setAdults(String adults) {
        Adults = adults;
    }

    public String getChild() {
        return Child;
    }

    public void setChild(String child) {
        Child = child;
    }

    public String getInf() {
        return Inf;
    }

    public void setInf(String inf) {
        Inf = inf;
    }

    public String isRoundTrip() {
        return IsRoundTrip;
    }

    public void setRoundTrip(String roundTrip) {
        IsRoundTrip = roundTrip;
    }

    public AgencyParams getAgencyParams() {
        return agencyParams;
    }

    public void setAgencyParams(AgencyParams agencyParams) {
        this.agencyParams = agencyParams;
    }


    public void setRequestInput() throws URISyntaxException {

        this.client = HttpClientBuilder.create().build();

        URI uri = new URIBuilder()
                .setScheme(getScheme())
                .setHost(getHost())
                .setPath(getPath())
                .build();
        this.httpost = new HttpPost(uri);

        StringEntity requestEntity = new StringEntity("{\n" +
                "\t\"DepartureCode\":\"" + getDepartureCode() + "\",\n" +
                "\t\"ArrivalCode\":\"" + getArrivalCode() + "\",\n" +
                "\t\"DepartTime\":\"" + getDepartTime() + "\",\n" +
                "\t\"ReturnTime\":\"" + getReturnTime() + "\",\n" +
                "\t\"Adults\":" + getAdults() + ",\n" +
                "\t\"Child\":" + getChild() + ",\n" +
                "\t\"Inf\":" + getInf() + ",\n" +
                "\t\"IsRoundTrip\":" + isRoundTrip() + ",\n" +
                "\t\"aId\":\"" + agencyParams.getaId() + "\"\n" +
                "}", ContentType.APPLICATION_JSON);
        this.httpost.setEntity(requestEntity);
    }


    public void mapping() {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

            JsonNode jsonNode = objectMapper.readTree(this.result_post.toString());

            assert jsonNode != null;
            this.setSearchFlightObject(objectMapper.readValue(jsonNode.toString().replaceAll("\"Class\"", "\"TicketClass\"" +
                    ""), SearchFlightObject.class));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}