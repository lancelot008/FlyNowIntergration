package FlynowAPI.SearchFlightAPI;

import java.util.List;

public class SegmentsObject {

    private String Airline;
    private String AirlineName;
    private String FlightNo;
    private String DepartureTime;
    private String ArriveTime;
    private DepartureObject Departure;
    private ArriveObject Arrive;
    private String Status;
    private String LayOver;
    private String Duration;
    private String TicketClass;
    private List<PricesObject> Prices;

    public SegmentsObject() {
    }

    public String getAirline() {
        return Airline;
    }

    public void setAirline(String airline) {
        Airline = airline;
    }

    public String getAirlineName() {
        return AirlineName;
    }

    public void setAirlineName(String airlineName) {
        AirlineName = airlineName;
    }

    public String getFlightNo() {
        return FlightNo;
    }

    public void setFlightNo(String flightNo) {
        FlightNo = flightNo;
    }

    public String getDepartureTime() {
        return DepartureTime;
    }

    public void setDepartureTime(String departureTime) {
        DepartureTime = departureTime;
    }

    public String getArriveTime() {
        return ArriveTime;
    }

    public void setArriveTime(String arriveTime) {
        ArriveTime = arriveTime;
    }

    public DepartureObject getDeparture() {
        return Departure;
    }

    public void setDeparture(DepartureObject departure) {
        Departure = departure;
    }

    public ArriveObject getArrive() {
        return Arrive;
    }

    public void setArrive(ArriveObject arrive) {
        Arrive = arrive;
    }


    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getLayOver() {
        return LayOver;
    }

    public void setLayOver(String layOver) {
        LayOver = layOver;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getTicketClass() {
        return TicketClass;
    }

    public void setTicketClass(String TicketClass) {
        this.TicketClass = TicketClass;
    }

    public List<PricesObject> getPrices() {
        return Prices;
    }

    public void setPrices(List<PricesObject> prices) {
        Prices = prices;
    }

}
