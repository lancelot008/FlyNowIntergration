package FlynowAPI.SearchFlightAPI;

import java.util.List;

public class OutboundObject {

    public OutboundObject() {
    }

    private double UId;
    private double Id;
    private double GroupCode;

    private List<SegmentsObject> Segments;

    private double TotalPrice;
    private double TotalDiscount;
    private String TotalTime;
    private PriceDetailObject PriceDetail;


    public double getUId() {
        return UId;
    }

    public void setUId(double UId) {
        this.UId = UId;
    }

    public double getId() {
        return Id;
    }

    public void setId(double id) {
        Id = id;
    }

    public double getGroupCode() {
        return GroupCode;
    }

    public void setGroupCode(double groupCode) {
        GroupCode = groupCode;
    }

    public List<SegmentsObject> getSegments() {
        return Segments;
    }

    public void setSegments(List<SegmentsObject> segments) {
        Segments = segments;
    }

    public double getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        TotalPrice = totalPrice;
    }

    public double getTotalDiscount() {
        return TotalDiscount;
    }

    public void setTotalDiscount(double totalDiscount) {
        TotalDiscount = totalDiscount;
    }

    public String getTotalTime() {
        return TotalTime;
    }

    public void setTotalTime(String totalTime) {
        TotalTime = totalTime;
    }

    public PriceDetailObject getPriceDetail() {
        return PriceDetail;
    }

    public void setPriceDetail(PriceDetailObject priceDetail) {
        PriceDetail = priceDetail;
    }
}
