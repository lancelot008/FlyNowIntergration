package FlynowAPI.SearchFlightAPI;

public class ListPriceDayOutboundObject {


    private String AirlineName;
    private String Date;
    private double Price;

    public ListPriceDayOutboundObject() {
    }

    public String getAirlineName() {
        return AirlineName;
    }

    public void setAirlineName(String airlineName) {
        AirlineName = airlineName;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }
}
