package FlynowAPI.SearchFlightAPI;

public class PriceDetailObject {
    public PriceDetailObject() {
    }

    private long Id;
    private String TicketClass;
    private double BasePriceAdult;
    private double BasePriceChild;
    private double BasePriceInf;
    private double FeeAdult;
    private double FeeChild;
    private double FeeInf;
    private double AirportFeeAdult;
    private double AirportFeeChild;
    private double AirportFeeInf;
    private double ServiceFeeAdult;
    private double ServiceFeeChild;
    private double ServiceFeeInf;
    private double Discount;
    private double TotalTaxPrice;
    private double TotalPrice;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getTicketClass() {
        return TicketClass;
    }

    public void setTicketClass(String ticketClass) {
        TicketClass = ticketClass;
    }

    public double getBasePriceAdult() {
        return BasePriceAdult;
    }

    public void setBasePriceAdult(double basePriceAdult) {
        BasePriceAdult = basePriceAdult;
    }

    public double getBasePriceChild() {
        return BasePriceChild;
    }

    public void setBasePriceChild(double basePriceChild) {
        BasePriceChild = basePriceChild;
    }

    public double getBasePriceInf() {
        return BasePriceInf;
    }

    public void setBasePriceInf(double basePriceInf) {
        BasePriceInf = basePriceInf;
    }

    public double getFeeAdult() {
        return FeeAdult;
    }

    public void setFeeAdult(double feeAdult) {
        FeeAdult = feeAdult;
    }

    public double getFeeChild() {
        return FeeChild;
    }

    public void setFeeChild(double feeChild) {
        FeeChild = feeChild;
    }

    public double getFeeInf() {
        return FeeInf;
    }

    public void setFeeInf(double feeInf) {
        FeeInf = feeInf;
    }

    public double getAirportFeeAdult() {
        return AirportFeeAdult;
    }

    public void setAirportFeeAdult(double airportFeeAdult) {
        AirportFeeAdult = airportFeeAdult;
    }

    public double getAirportFeeChild() {
        return AirportFeeChild;
    }

    public void setAirportFeeChild(double airportFeeChild) {
        AirportFeeChild = airportFeeChild;
    }

    public double getAirportFeeInf() {
        return AirportFeeInf;
    }

    public void setAirportFeeInf(double airportFeeInf) {
        AirportFeeInf = airportFeeInf;
    }

    public double getServiceFeeAdult() {
        return ServiceFeeAdult;
    }

    public void setServiceFeeAdult(double serviceFeeAdult) {
        ServiceFeeAdult = serviceFeeAdult;
    }

    public double getServiceFeeChild() {
        return ServiceFeeChild;
    }

    public void setServiceFeeChild(double serviceFeeChild) {
        ServiceFeeChild = serviceFeeChild;
    }

    public double getServiceFeeInf() {
        return ServiceFeeInf;
    }

    public void setServiceFeeInf(double serviceFeeInf) {
        ServiceFeeInf = serviceFeeInf;
    }

    public double getDiscount() {
        return Discount;
    }

    public void setDiscount(double discount) {
        Discount = discount;
    }

    public double getTotalTaxPrice() {
        return TotalTaxPrice;
    }

    public void setTotalTaxPrice(double totalTaxPrice) {
        TotalTaxPrice = totalTaxPrice;
    }

    public double getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        TotalPrice = totalPrice;
    }
}
