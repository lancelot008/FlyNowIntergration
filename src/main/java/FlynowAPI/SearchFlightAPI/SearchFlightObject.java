package FlynowAPI.SearchFlightAPI;

import java.util.List;

public class SearchFlightObject {



    public SearchFlightObject(){}


    private List<DataObject> Data;
    private boolean IsError;
    private int ErrorCode;
    private String Error;

    public List<DataObject> getData() {
        return Data;
    }

    public void setData(List<DataObject> data) {
        Data = data;
    }

    public int getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(int errorCode) {
        ErrorCode = errorCode;
    }

    public String getError() {
        return Error;
    }

    public void setError(String error) {
        Error = error;
    }

    public boolean getIsError() {
        return IsError;
    }

    public void setIsError(boolean isError) {
        IsError = isError;
    }
}
