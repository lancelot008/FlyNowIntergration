package FlynowAPI.SearchFlightAPI;

import java.util.List;

public class DataObject {


    public DataObject(){};


    private RuleCombine RuleCombine;
    private String SourcePrice;
    private DepartureObject Departure;
    private ArrivalObject Arrival;

    private int MaxTransit;
    private List<ListPriceDayOutboundObject> ListPriceDayOutbound;

    private List<OutboundObject> Outbound;

    private List<ListPriceDayInboundObject> ListPriceDayInbound;
    private List<InboundObject>Inbound;

    public FlynowAPI.SearchFlightAPI.RuleCombine getRuleCombine() {
        return RuleCombine;
    }

    public void setRuleCombine(FlynowAPI.SearchFlightAPI.RuleCombine ruleCombine) {
        RuleCombine = ruleCombine;
    }

    public String getSourcePrice() {
        return SourcePrice;
    }

    public void setSourcePrice(String sourcePrice) {
        SourcePrice = sourcePrice;
    }

    public DepartureObject getDeparture() {
        return Departure;
    }

    public void setDeparture(DepartureObject departure) {
        Departure = departure;
    }

    public ArrivalObject getArrival() {
        return Arrival;
    }

    public void setArrival(ArrivalObject arrival) {
        Arrival = arrival;
    }

    public int getMaxTransit() {
        return MaxTransit;
    }

    public void setMaxTransit(int maxTransit) {
        MaxTransit = maxTransit;
    }

    public List<ListPriceDayOutboundObject> getListPriceDayOutbound() {
        return ListPriceDayOutbound;
    }

    public void setListPriceDayOutbound(List<ListPriceDayOutboundObject> listPriceDayOutbound) {
        ListPriceDayOutbound = listPriceDayOutbound;
    }

    public List<OutboundObject> getOutbound() {
        return Outbound;
    }

    public void setOutbound(List<OutboundObject> outbound) {
        Outbound = outbound;
    }


    public List<ListPriceDayInboundObject> getListPriceDayInbound() {
        return ListPriceDayInbound;
    }

    public void setListPriceDayInbound(List<ListPriceDayInboundObject> listPriceDayInbound) {
        ListPriceDayInbound = listPriceDayInbound;
    }

    public List<InboundObject> getInbound() {
        return Inbound;
    }

    public void setInbound(List<InboundObject> inbound) {
        Inbound = inbound;
    }
}
