package FlynowAPI;

import FlynowAPI.Authentication.Authentication;
import FlynowAPI.Authentication.AuthenticationObject;
import FlynowAPI.CreateOrder.*;
import FlynowAPI.GetAddOnService.GetAddOnService;
import FlynowAPI.GetAddOnService.GetAddOnServiceObject;
import FlynowAPI.SearchFlightAPI.SearchFlight;
import FlynowAPI.SearchFlightAPI.SearchFlightObject;
import vnptpay.message.common.CollectionErrorCode;

import java.util.List;

public class FlyNowProcess {

    private AgencyParams agencyParams;
    private String host;
//    private String path;

    public FlyNowProcess(AgencyParams agencyParams, String host) {
        this.agencyParams = agencyParams;
//        this.host = host;
    }

    public AgencyParams getAgencyParams() {
        return agencyParams;
    }

    public void setAgencyParams(AgencyParams agencyParams) {
        this.agencyParams = agencyParams;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }



    public CollectionErrorCode authentication(AuthenticationObject response, String path){

        CollectionErrorCode errorCode = CollectionErrorCode.UNDEFINED;
        AuthenticationObject authenticationObject = null;


        Authentication authentication = new Authentication(agencyParams);
        authentication.setHost(getHost());
        authentication.setPath(path);

        try {
            authentication.request();
            authenticationObject = authentication.getAuthenticationObject();

            if (authenticationObject != null){
                if (!authenticationObject.getIsError()){
                    errorCode = CollectionErrorCode.OK;
                    response.setData(authenticationObject.getData());
                    response.setError(authenticationObject.getError());
                    response.setErrorCode(authenticationObject.getErrorCode());
                    response.setIsError(authenticationObject.getIsError());
                }else {
                    errorCode = CollectionErrorCode.NOT_FOUND_DATA;
                }
            }
        }catch (Exception e){
            errorCode = CollectionErrorCode.NOT_FOUND_DATA;
            e.printStackTrace();
        }
        return errorCode;
    }

    public CollectionErrorCode searchFlight(SearchFlightObject response, String departureCode, String arrivalCode, String departTime, String returnTime, String adults, String child, String inf, String isRoundTrip, AgencyParams agencyParams, String path){
        CollectionErrorCode errorCode = CollectionErrorCode.UNDEFINED;
        SearchFlightObject searchFlightObject = null;


        SearchFlight searchFlight = new SearchFlight(departureCode, arrivalCode, departTime, returnTime, adults, child, inf, isRoundTrip, agencyParams);
        searchFlight.setHost(getHost());
        searchFlight.setPath(path);

        try {
            searchFlight.request();
            searchFlightObject = searchFlight.getSearchFlightObject();


            if (searchFlightObject != null){
                if (!searchFlightObject.getIsError()){
                    errorCode = CollectionErrorCode.OK;
                    response.setData(searchFlightObject.getData());
                    response.setError(searchFlightObject.getError());
                    response.setErrorCode(searchFlightObject.getErrorCode());
                    response.setIsError(searchFlightObject.getIsError());
                }else {
                    errorCode = CollectionErrorCode.NOT_FOUND_DATA;
                }
            }

        }catch (Exception e){
            errorCode = CollectionErrorCode.ACCOUNT_NOT_EXITS;
            e.printStackTrace();
        }

        return errorCode;

    }

    public CollectionErrorCode createOrder(CreateOrderObject response, List<GuestInforObject> guestInfor, ContactInfoObject contactInfo, FnSelectObject fnSelect, AgencyParams agencyParams, String orderId, String authenPath, String path){
        CollectionErrorCode errorCode = CollectionErrorCode.UNDEFINED;
        CreateOrderObject createOrderObject = null;

        AuthenticationObject authenticationInfo = new AuthenticationObject();
        CollectionErrorCode authenError = authentication(authenticationInfo, authenPath);
        if (authenError != CollectionErrorCode.NOT_FOUND_DATA && authenError != CollectionErrorCode.ACCOUNT_NOT_EXITS){
            CreateOrder createOrder = new CreateOrder(guestInfor, contactInfo, fnSelect, agencyParams, orderId, authenticationInfo.getData().getAccess_token());
            createOrder.setPath(path);

            try{

                createOrder.request();
                createOrderObject = createOrder.getCreateOrderObject();
                if (createOrderObject != null){
                    errorCode = CollectionErrorCode.OK;
                    response.setErrorMessages(createOrderObject.getErrorMessages());
                    response.setHoldId(createOrderObject.getHoldId());
                    response.setOrderId(createOrderObject.getOrderId());
                    response.setPriceDetail(createOrderObject.getPriceDetail());
                    response.setStatusCode(createOrderObject.getStatusCode());

                }else {

                    errorCode = CollectionErrorCode.NOT_FOUND_DATA;
                }

            }catch (Exception e){

                errorCode = CollectionErrorCode.NOT_FOUND_DATA;
                e.printStackTrace();
            }


        }else {

            errorCode = CollectionErrorCode.ACCOUNT_NOT_EXITS;
        }


        return errorCode;
    }


    public CollectionErrorCode getAddOnService(GetAddOnServiceObject response, long UID, long outboundID, long outboundPriceID, int itineraryType, long inboundID, long inboundPriceID, String path){
        CollectionErrorCode errorCode = CollectionErrorCode.UNDEFINED;
        GetAddOnServiceObject getAddOnServiceObject = null;


        GetAddOnService getAddOnService = new GetAddOnService(UID, outboundID, outboundPriceID, itineraryType, inboundID,inboundPriceID, agencyParams);
        getAddOnService.setPath(path);

        try{

            getAddOnService.request();

            getAddOnServiceObject = getAddOnService.getGetAddOnServiceObject();

            if (getAddOnServiceObject != null){

                response.setDepartAddons(getAddOnServiceObject.getDepartAddons());
                response.setReturnAddons(getAddOnServiceObject.getReturnAddons());

            }else {
                errorCode = CollectionErrorCode.NOT_FOUND_DATA;
            }

        }catch (Exception e){
            errorCode = CollectionErrorCode.NOT_FOUND_DATA;
            e.printStackTrace();
        }
        return errorCode;
    }
}
