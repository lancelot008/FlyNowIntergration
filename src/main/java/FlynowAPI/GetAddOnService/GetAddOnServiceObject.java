package FlynowAPI.GetAddOnService;

import java.util.List;

public class GetAddOnServiceObject {

    private List<DepartAddonsObject> DepartAddons;
    private List<ReturnAddonsObject> ReturnAddons;

    public GetAddOnServiceObject() {
    }

    public List<DepartAddonsObject> getDepartAddons() {
        return DepartAddons;
    }

    public void setDepartAddons(List<DepartAddonsObject> departAddons) {
        DepartAddons = departAddons;
    }

    public List<ReturnAddonsObject> getReturnAddons() {
        return ReturnAddons;
    }

    public void setReturnAddons(List<ReturnAddonsObject> returnAddons) {
        ReturnAddons = returnAddons;
    }
}
