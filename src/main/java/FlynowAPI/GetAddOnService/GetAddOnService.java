package FlynowAPI.GetAddOnService;

import FlynowAPI.AgencyParams;
import FlynowAPI.MappingObjects.GetRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class GetAddOnService extends GetRequest{

    private GetAddOnServiceObject GetAddOnServiceObject;
    private long UID;
    private long OutboundID;
    private long OutboundPriceID;
    private int ItineraryType;
    private long InboundID;
    private long InboundPriceID;
    private AgencyParams agencyParams;

    public GetAddOnService(long UID, long outboundID, long outboundPriceID, int itineraryType, long inboundID, long inboundPriceID, AgencyParams agencyParams) {
        this.UID = UID;
        OutboundID = outboundID;
        OutboundPriceID = outboundPriceID;
        ItineraryType = itineraryType;
        InboundID = inboundID;
        InboundPriceID = inboundPriceID;
        this.agencyParams = agencyParams;
    }

    public FlynowAPI.GetAddOnService.GetAddOnServiceObject getGetAddOnServiceObject() {
        return GetAddOnServiceObject;
    }

    public void setGetAddOnServiceObject(FlynowAPI.GetAddOnService.GetAddOnServiceObject getAddOnServiceObject) {
        GetAddOnServiceObject = getAddOnServiceObject;
    }

    public long getUID() {
        return UID;
    }

    public void setUID(long UID) {
        this.UID = UID;
    }

    public long getOutboundID() {
        return OutboundID;
    }

    public void setOutboundID(long outboundID) {
        OutboundID = outboundID;
    }

    public long getOutboundPriceID() {
        return OutboundPriceID;
    }

    public void setOutboundPriceID(long outboundPriceID) {
        OutboundPriceID = outboundPriceID;
    }

    public int getItineraryType() {
        return ItineraryType;
    }

    public void setItineraryType(int itineraryType) {
        ItineraryType = itineraryType;
    }

    public long getInboundID() {
        return InboundID;
    }

    public void setInboundID(long inboundID) {
        InboundID = inboundID;
    }

    public long getInboundPriceID() {
        return InboundPriceID;
    }

    public void setInboundPriceID(long inboundPriceID) {
        InboundPriceID = inboundPriceID;
    }

    public AgencyParams getAgencyParams() {
        return agencyParams;
    }

    public void setAgencyParams(AgencyParams agencyParams) {
        this.agencyParams = agencyParams;
    }

    public void setRequestInput() throws URISyntaxException {

        this.client = HttpClientBuilder.create().build();
        URI uri = new URIBuilder()
                .setScheme(getScheme())
                .setHost(getHost())
                .setPath(getPath())
                .addParameter("UID", Long.toString(getUID()))
                .addParameter("OutboundID", Long.toString(getOutboundID()))
                .addParameter("OutboundPriceID", Long.toString(getOutboundID()))
                .addParameter("ItineraryType", Integer.toString(getItineraryType()))
                .addParameter("InboundID", Long.toString(getInboundID()))
                .addParameter("InboundPriceID", Long.toString(getInboundPriceID()))
                .addParameter("aId", agencyParams.getaId())
                .build();
        this.httpGet = new HttpGet(uri);
    }

    public void mapping() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            JsonNode jsonNode = objectMapper.readTree(this.stringBuffer.toString());
            assert jsonNode != null;
            this.setGetAddOnServiceObject(objectMapper.readValue(jsonNode.toString(), GetAddOnServiceObject.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
