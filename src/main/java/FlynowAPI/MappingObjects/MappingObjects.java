package FlynowAPI.MappingObjects;

import java.net.URISyntaxException;

public abstract class MappingObjects {

    private String scheme;
    private String host;
    private String path;

    public MappingObjects() {
    }

    public void request() throws URISyntaxException {

        setRequestInput();
        executeRequest();
        mapping();

    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setRequestInput() throws URISyntaxException {
        System.out.println("method for setting inputs for request");
    }

    public void executeRequest(){
        System.out.println("method for executing request");
    }

    public void mapping(){
        System.out.println("method for mapping objects");
    }

}
