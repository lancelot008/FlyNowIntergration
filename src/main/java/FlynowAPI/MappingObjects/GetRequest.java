package FlynowAPI.MappingObjects;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GetRequest extends MappingObjects{

    protected HttpGet httpGet;
    protected HttpClient client;
    protected StringBuffer stringBuffer;

    public GetRequest() {
    }

    public void executeRequest() {
        try {
            HttpResponse response = client.execute(httpGet);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            stringBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
