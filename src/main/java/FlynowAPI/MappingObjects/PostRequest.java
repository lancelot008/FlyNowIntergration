package FlynowAPI.MappingObjects;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PostRequest extends MappingObjects{


    protected HttpClient client;
    protected HttpPost httpost;
    protected StringBuffer result_post;

    public PostRequest() {
    }

    public void executeRequest(){

        try {
            HttpResponse response_post = this.client.execute(httpost);
            BufferedReader rd_post = new BufferedReader(new InputStreamReader(response_post.getEntity().getContent()));

            this.result_post = new StringBuffer();
            String line_post;
            while ((line_post = rd_post.readLine()) != null) {
                result_post.append(line_post);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
