package FlynowAPI;

import FlynowAPI.Authentication.Authentication;
import FlynowAPI.AutoSuggestAirportAPI.AutoSuggestAirport;
import FlynowAPI.BestPrice.BestPrice;
import FlynowAPI.CreateOrder.*;
import FlynowAPI.GetAddOnService.GetAddOnService;
import FlynowAPI.SearchFlightAPI.SearchFlight;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class ExecuteRequests {

    public ExecuteRequests() {

    }

    public static void main(String[] args) throws URISyntaxException {

        AgencyParams agencyParams = new AgencyParams("VNPT", "phucphuong1293@gmail.com", "123456", "A3EFDFABA8653DF2342E8DAC29B51AF0");

//        TODO: cho api gợi ý chuyến bay
        AutoSuggestAirport autoSuggestAirport = new AutoSuggestAirport("Ha Noi", agencyParams);
        autoSuggestAirport.setScheme("https");
        autoSuggestAirport.setHost("api.flynow.vn");
        autoSuggestAirport.setPath("/api/Search/AutoSuggestAirport");
//        autoSuggestAirport.request();
//        System.out.println(autoSuggestAirport.getAutoSuggestAirportObjectList().get(0).getCityId());

//        TODO: cho api tìm chuyến
        SearchFlight searchFlight = new SearchFlight("HAN", "DAD", "2018-02-24", "2018-02-26", "1", "1", "1", "true", agencyParams);
        searchFlight.setScheme("https");
        searchFlight.setHost("api.flynow.vn");
        searchFlight.setPath("/api/Search/Flights");
//        searchFlight.request();
//        System.out.println(searchFlight.getSearchFlightObject().getData().get(0).getArrival().getAirportCode());


//        TODO: api GetAddonService

        GetAddOnService getAddOnService = new GetAddOnService(52891476, 8527523, 491115855, 2, 8527643, 491116176, agencyParams);

        getAddOnService.setScheme("https");
        getAddOnService.setHost("api.flynow.vn");
        getAddOnService.setPath("/api/Search/GetAddOnService");
        getAddOnService.request();
        System.out.println(getAddOnService.getGetAddOnServiceObject().getDepartAddons().get(0).getAmount());

//        TODO: cho api tìm vé giá tốt nhất
        BestPrice bestPrice = new BestPrice("HAN", "SGN", "2018-02-23", "2018-02-25", "", "true", agencyParams);
        bestPrice.setScheme("https");
        bestPrice.setHost("api.flynow.vn");
        bestPrice.setPath("/api/Minprice/GetMinPrice");
//        bestPrice.request();
//        System.out.println(bestPrice.getBestPriceObject().getOutbound().get(0).getDate());

//        TODO: api authentication
        Authentication authentication = new Authentication(agencyParams);
        authentication.setScheme("https");
        authentication.setHost("api.flynow.vn");
        authentication.setPath("/api/Account/Authen");
        authentication.request();
//        System.out.print(authentication.getAuthenticationObject().getData().getAccess_token());


//        TODO: test api create order
        String orderId = "1"; // Phụ thuộc vào db của VNPT
        List<GuestInforObject> guestInforObjectList = new ArrayList<>();
        ContactInfoObject contactInfoObject = new ContactInfoObject("Hồ Phúc Phương", "Minh khai", "0987654321", "testapi@gmail.com", null, null, null, null, null);
        FnSelectObject fnSelectObject = new FnSelectObject(52824741, 8166161, 490048016, 2, 8166183, 490048065);
        BaggageDepartObject baggageDepartObject = new BaggageDepartObject("VJ", "15", null, null, "154000", "15");
        BaggageReturnObject baggageReturnObject = new BaggageReturnObject(null, null, null, null, null, "0");
        GuestInforObject guestInforObject = new GuestInforObject(1, null, 1, "Ông", 0, "Ho", "Phuc", "Phuong", null, null,
                null, null, null, null, baggageDepartObject, baggageReturnObject);
        guestInforObjectList.add(guestInforObject);
        CreateOrder createOrder = new CreateOrder(guestInforObjectList, contactInfoObject, fnSelectObject, agencyParams, orderId, authentication.getAuthenticationObject().getData().getAccess_token());

        createOrder.setScheme("https");
        createOrder.setHost("api.flynow.vn");
        createOrder.setPath("/api/HoldBooking/CreateOrder");

        createOrder.request();
        System.out.print(createOrder.getCreateOrderObject().getErrorMessages());
    }
}
