package FlynowAPI;

public class AgencyParams {

    private String aId;
    private String account;
    private String password;
    private String client_key;


//    public AgencyParams(){
//        this.aId = "VNPT";
//        this.account = "phucphuong1293@gmail.com";
//        this.password = "123456";
//        this.client_key = "A3EFDFABA8653DF2342E8DAC29B51AF0";
//    }

    public AgencyParams(String aId, String account, String password, String client_key) {
        this.aId = aId;
        this.account = account;
        this.password = password;
        this.client_key = client_key;
    }

    public String getaId() {
        return aId;
    }

    public void setaId(String aId) {
        this.aId = aId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClient_key() {
        return client_key;
    }

    public void setClient_key(String client_key) {
        this.client_key = client_key;
    }
}
