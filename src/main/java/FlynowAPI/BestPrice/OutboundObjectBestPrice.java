package FlynowAPI.BestPrice;

public class OutboundObjectBestPrice {

    private String Date;
    private PriceObjectBestPrice Price;

    public OutboundObjectBestPrice() {
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public PriceObjectBestPrice getPrice() {
        return Price;
    }

    public void setPrice(PriceObjectBestPrice price) {
        Price = price;
    }
}
