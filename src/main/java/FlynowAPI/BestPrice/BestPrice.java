package FlynowAPI.BestPrice;

import FlynowAPI.AgencyParams;
import FlynowAPI.MappingObjects.GetRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class BestPrice extends GetRequest {

    private BestPriceObject bestPriceObject;
    private String departCode;
    private String arrivalCode;
    private String departTime;
    private String returnTime;
    private String source;
    private String isRoundTrip;
    private AgencyParams agencyParams;

    public BestPrice(String departCode, String arrivalCode, String departTime, String returnTime, String source, String isRoundTrip, AgencyParams agencyParams) {
        this.departCode = departCode;
        this.arrivalCode = arrivalCode;
        this.departTime = departTime;
        this.returnTime = returnTime;
        this.source = source;
        this.isRoundTrip = isRoundTrip;
        this.agencyParams = agencyParams;
    }

    public BestPriceObject getBestPriceObject() {
        return bestPriceObject;
    }

    public void setBestPriceObject(BestPriceObject bestPriceObject) {
        this.bestPriceObject = bestPriceObject;
    }

    public String getDepartCode() {
        return departCode;
    }

    public void setDepartCode(String departCode) {
        this.departCode = departCode;
    }

    public String getArrivalCode() {
        return arrivalCode;
    }

    public void setArrivalCode(String arrivalCode) {
        this.arrivalCode = arrivalCode;
    }

    public String getDepartTime() {
        return departTime;
    }

    public void setDepartTime(String departTime) {
        this.departTime = departTime;
    }

    public String getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(String returnTime) {
        this.returnTime = returnTime;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getIsRoundTrip() {
        return isRoundTrip;
    }

    public void setIsRoundTrip(String isRoundTrip) {
        this.isRoundTrip = isRoundTrip;
    }

    public void setRequestInput() throws URISyntaxException {
        this.client = HttpClientBuilder.create().build();
        URI uri = new URIBuilder()
                .setScheme(getScheme())
                .setHost(getHost())
                .setPath(getPath())
                .addParameter("departCode", getDepartCode())
                .addParameter("arrivalCode", getArrivalCode())
                .addParameter("departTime", getDepartTime())
                .addParameter("returnTime", getReturnTime())
                .addParameter("source", getSource())
                .addParameter("isRoundTrip", getIsRoundTrip())
                .addParameter("aId", agencyParams.getaId())
                .build();
        this.httpGet = new HttpGet(uri);
    }

    public void mapping() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            JsonNode jsonNode = objectMapper.readTree(this.stringBuffer.toString());
            assert jsonNode != null;
            this.setBestPriceObject(objectMapper.readValue(jsonNode.toString(), BestPriceObject.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
