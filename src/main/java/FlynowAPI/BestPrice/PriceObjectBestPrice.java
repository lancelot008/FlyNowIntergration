package FlynowAPI.BestPrice;

public class PriceObjectBestPrice {

    private long Id;
    private String TicketClass;
    private double BasePriceAdult;
    private double BasePriceChild;
    private double BasePriceInf;
    private double FeeAdult;
    private double FeeChild;
    private double FeeInf;
    private double TaxAdult;
    private double TaxChild;
    private double TaxInf;
    private double AirportFeeAdult;
    private double AirportFeeChild;
    private double AirportFeeInf;
    private double ServiceFeeAdult;
    private double ServiceFeeChild;
    private double ServiceFeeInf;
    private double PriceVN;
    private double PriceVJ;
    private double PriceBL;
    private double MinPrice;
    private double BasePriceVN;
    private double BasePriceVJ;
    private double BasePriceBL;
    private double MinBasePrice;
    private double TotalPrice;


    public PriceObjectBestPrice() {
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getTicketClass() {
        return TicketClass;
    }

    public void setTicketClass(String ticketClass) {
        TicketClass = ticketClass;
    }

    public double getBasePriceAdult() {
        return BasePriceAdult;
    }

    public void setBasePriceAdult(double basePriceAdult) {
        BasePriceAdult = basePriceAdult;
    }

    public double getBasePriceChild() {
        return BasePriceChild;
    }

    public void setBasePriceChild(double basePriceChild) {
        BasePriceChild = basePriceChild;
    }

    public double getBasePriceInf() {
        return BasePriceInf;
    }

    public void setBasePriceInf(double basePriceInf) {
        BasePriceInf = basePriceInf;
    }

    public double getFeeAdult() {
        return FeeAdult;
    }

    public void setFeeAdult(double feeAdult) {
        FeeAdult = feeAdult;
    }

    public double getFeeChild() {
        return FeeChild;
    }

    public void setFeeChild(double feeChild) {
        FeeChild = feeChild;
    }

    public double getFeeInf() {
        return FeeInf;
    }

    public void setFeeInf(double feeInf) {
        FeeInf = feeInf;
    }

    public double getTaxAdult() {
        return TaxAdult;
    }

    public void setTaxAdult(double taxAdult) {
        TaxAdult = taxAdult;
    }

    public double getTaxChild() {
        return TaxChild;
    }

    public void setTaxChild(double taxChild) {
        TaxChild = taxChild;
    }

    public double getTaxInf() {
        return TaxInf;
    }

    public void setTaxInf(double taxInf) {
        TaxInf = taxInf;
    }

    public double getAirportFeeAdult() {
        return AirportFeeAdult;
    }

    public void setAirportFeeAdult(double airportFeeAdult) {
        AirportFeeAdult = airportFeeAdult;
    }

    public double getAirportFeeChild() {
        return AirportFeeChild;
    }

    public void setAirportFeeChild(double airportFeeChild) {
        AirportFeeChild = airportFeeChild;
    }

    public double getAirportFeeInf() {
        return AirportFeeInf;
    }

    public void setAirportFeeInf(double airportFeeInf) {
        AirportFeeInf = airportFeeInf;
    }

    public double getServiceFeeAdult() {
        return ServiceFeeAdult;
    }

    public void setServiceFeeAdult(double serviceFeeAdult) {
        ServiceFeeAdult = serviceFeeAdult;
    }

    public double getServiceFeeChild() {
        return ServiceFeeChild;
    }

    public void setServiceFeeChild(double serviceFeeChild) {
        ServiceFeeChild = serviceFeeChild;
    }

    public double getServiceFeeInf() {
        return ServiceFeeInf;
    }

    public void setServiceFeeInf(double serviceFeeInf) {
        ServiceFeeInf = serviceFeeInf;
    }

    public double getPriceVN() {
        return PriceVN;
    }

    public void setPriceVN(double priceVN) {
        PriceVN = priceVN;
    }

    public double getPriceVJ() {
        return PriceVJ;
    }

    public void setPriceVJ(double priceVJ) {
        PriceVJ = priceVJ;
    }

    public double getPriceBL() {
        return PriceBL;
    }

    public void setPriceBL(double priceBL) {
        PriceBL = priceBL;
    }

    public double getMinPrice() {
        return MinPrice;
    }

    public void setMinPrice(double minPrice) {
        MinPrice = minPrice;
    }

    public double getBasePriceVN() {
        return BasePriceVN;
    }

    public void setBasePriceVN(double basePriceVN) {
        BasePriceVN = basePriceVN;
    }

    public double getBasePriceVJ() {
        return BasePriceVJ;
    }

    public void setBasePriceVJ(double basePriceVJ) {
        BasePriceVJ = basePriceVJ;
    }

    public double getBasePriceBL() {
        return BasePriceBL;
    }

    public void setBasePriceBL(double basePriceBL) {
        BasePriceBL = basePriceBL;
    }

    public double getMinBasePrice() {
        return MinBasePrice;
    }

    public void setMinBasePrice(double minBasePrice) {
        MinBasePrice = minBasePrice;
    }

    public double getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        TotalPrice = totalPrice;
    }
}
