package FlynowAPI.BestPrice;

import java.util.List;

public class BestPriceObject {

    private String SearchId;
    private String Source;
    private String Airline;
    private String Departure;
    private String Arrival;
    private List<OutboundObjectBestPrice> Outbound;
    private List<InboundObjectBestPrice> Inbound;

    public BestPriceObject() {
    }

    public String getSearchId() {
        return SearchId;
    }

    public void setSearchId(String searchId) {
        SearchId = searchId;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }

    public String getAirline() {
        return Airline;
    }

    public void setAirline(String airline) {
        Airline = airline;
    }

    public String getDeparture() {
        return Departure;
    }

    public void setDeparture(String departure) {
        Departure = departure;
    }

    public String getArrival() {
        return Arrival;
    }

    public void setArrival(String arrival) {
        Arrival = arrival;
    }

    public List<OutboundObjectBestPrice> getOutbound() {
        return Outbound;
    }

    public void setOutbound(List<OutboundObjectBestPrice> outbound) {
        Outbound = outbound;
    }

    public List<InboundObjectBestPrice> getInbound() {
        return Inbound;
    }

    public void setInbound(List<InboundObjectBestPrice> inbound) {
        Inbound = inbound;
    }
}
